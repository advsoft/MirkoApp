import React, { Component } from 'react';
import { Root, StyleProvider } from 'native-base';
import { create } from 'mobx-persist';
import { AsyncStorage, NativeModules } from 'react-native';
import { observer } from 'mobx-react';
import { StackRouter } from './navigation/Router';
import './config/reactotron';
import getTheme from './native-base-theme/components';
import authStore from './stores/authStore';
import mirkoStore from './stores/mirkoStore';
import profileStore from './stores/profileStore';
import { setRootViewBackgroundColor } from 'react-native-root-view-background';
import colors from './config/colors';


const hydrate = create({ storage: AsyncStorage });
@observer
export default class Main extends Component {
  state = {
    rehydrated: false,
  };

  componentDidMount() {
    setRootViewBackgroundColor(colors.background);
    this.hydrateStores();
  }

  async hydrateStores() {
    await hydrate('authStore', authStore);
    await hydrate('mirkoStore', mirkoStore);
    await hydrate('profileStore', profileStore);
    this.setState({ rehydrated: true });
  }

  render() {
    if (!this.state.rehydrated) {
      return null;
    }
    return (
      <StyleProvider style={getTheme()}>
        <Root>
          <StackRouter/>
        </Root>
      </StyleProvider>);
  }
}
