import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { Button, Form, Input, Item, Label } from 'native-base';
import colors from '../config/colors';

export default class Auth extends Component {
  state = {
    username: '',
    password: '',
    hasError: false,
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.state.hasError = true;
    }
  }

  render() {
    const { username, password, hasError } = this.state;

    return (
      <View>
        <Text style={styles.headerText}>Sign In</Text>
        <Text style={styles.errorText}>{this.props.error}</Text>
        <Form>
          <Item floatingLabel error={hasError}>
            <Label>Username</Label>
            <Input
              onChangeText={value => this.setState({ username: value, hasError: false })}
              value={username}
              autoCorrect={false}
              autoCapitalize="none"
            />
          </Item>
          <Item floatingLabel last error={hasError}>
            <Label>Password</Label>
            <Input
              onChangeText={value => this.setState({ password: value, hasError: false })}
              value={password}
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry
            />
          </Item>
        </Form>
        <Button
          style={styles.signInButton}
          success
          onPress={() => this.props.onLogin(username, password)}
        >
          <Text style={styles.buttonText}>
            Zaloguj
          </Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerText: {
    marginTop: 10,
    textAlign: 'center',
    fontSize: 30,
    color: colors.textColor,
  },
  signInButton: {
    marginTop: 20,
    marginHorizontal: 10,
  },
  buttonText: {
    color: 'white',
    flex: 1,
    textAlign: 'center',
  },
  errorText: {
    marginTop: 10,
    textAlign: 'center',
    fontSize: 16,
    color: colors.textErrorColor,
  },
});

Auth.propTypes = {
  onLogin: PropTypes.func,
  error: PropTypes.string,
};

Auth.defaultProps = {
  onLogin: () => null,
};
