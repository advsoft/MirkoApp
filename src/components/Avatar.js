import React, { Component } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';
import colors from '../config/colors';


export default function Avatar({ avatarUrl, authorSex }) {
  const sexBarStyle = [styles.sexBar];
  if (authorSex === 'male') {
    sexBarStyle.push({ backgroundColor: colors.maleColor });
  }
  if (authorSex === 'female') {
    sexBarStyle.push({ backgroundColor: colors.femaleColor });
  }
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{ uri: avatarUrl }}/>
      <View style={sexBarStyle}/>
    </View>);
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 2,
    overflow: 'hidden',
  },
  image: {
    width: 36,
    height: 36,
  },
  sexBar: {
    height: 3,
    backgroundColor: 'white',
  },
});

Avatar.propTypes = {
  avatarUrl: PropTypes.string,
  authorSex: PropTypes.string,
};
