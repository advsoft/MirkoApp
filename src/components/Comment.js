import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';
import { Button } from 'native-base';
import 'moment/locale/pl';
import Content from './Content';

export default function Comment(props) {
  const { comment, maxHeight } = props;
  return (
    <View style={styles.container}>
      <Content {...props} entry={comment} maxHeight={maxHeight}/>
    </View>);
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
  },
});

Comment.propTypes = {
  comment: PropTypes.object,
  maxHeight: PropTypes.number,
  onPressImage: PropTypes.func,
  onPressYoutube: PropTypes.func,
  onPressGif: PropTypes.func,
  onPressGfy: PropTypes.func,
  onTagPress: PropTypes.func,
  onUserPress: PropTypes.func,
  onUrlPress: PropTypes.func,
  onVoteToggle: PropTypes.func,
  canVote: PropTypes.bool,
};

