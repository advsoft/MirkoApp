import React, { Component } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';
import { bind } from 'decko';
import Comment from './Comment';
import colors from '../config/colors';

export default class CommentsList extends Component {
  @bind
  renderItem({ item }) {
    return (
      <View style={{ marginLeft: 15 }}>
        <Comment {...this.props} comment={item} maxHeight={150}/>
      </View>);
  }

  render() {
    return (<FlatList
      data={this.props.comments}
      renderItem={this.renderItem}
      keyExtractor={item => item.id}
      renderScrollComponent={() => <View/>}
      removeClippedSubviews={false}
      initialNumToRender={this.props.comments.length}
      ItemSeparatorComponent={() => <View style={styles.separator}/>}
    />);
  }
}

const styles = StyleSheet.create({

  separator: {
    height: 1,
    flex: 1,
    backgroundColor: colors.inactiveDark,
    marginVertical: 5,
  },
});

CommentsList.propTypes = {
  comments: PropTypes.array,
  onPressImage: PropTypes.func,
  onPressYoutube: PropTypes.func,
  onPressGif: PropTypes.func,
  onPressGfy: PropTypes.func,
  onTagPress: PropTypes.func,
  onUserPress: PropTypes.func,
  onUrlPress: PropTypes.func,
  onEntryPress: PropTypes.func,
  onVoteToggle: PropTypes.func,
  canVote: PropTypes.bool,
};
