import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Animated, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import moment from 'moment';
import 'moment/locale/pl';
import HTMLView from 'react-native-htmlview';
import { Button } from 'native-base';
import PropTypes from 'prop-types';
import { bind } from 'decko';
import colors from '../config/colors';
import Avatar from './Avatar';
import Embed from './Embed';

export default class Content extends Component {
  state = {
    shouldExpand: false,
    isExpanded: false,
    contentWidth: 0,
    contentHeight: 0,
    contentMaxHeight: new Animated.Value(0),
  };

  componentWillMount() {
    this.state.contentMaxHeight = new Animated.Value(this.props.maxHeight);
  }

  @bind
  expandContent() {
    Animated.timing(this.state.contentMaxHeight, {
      toValue: this.state.contentHeight,
      duration: 210,
    }).start();
    this.setState({ isExpanded: true });
  }

  renderExpand() {
    if (!this.state.shouldExpand || this.state.isExpanded) {
      return null;
    }
    const containerDynamicStyle = {
      top: this.props.maxHeight - 45,
      width: this.state.contentWidth,
    };
    const gradientDynamicStyle = {
      width: this.state.contentWidth,
    };
    return (
      <View style={[styles.expandContainerStyle, containerDynamicStyle]}>
        <Image
          source={require('../../assets/transparent_shadow.png')}
          style={[styles.expandGradientStyle, gradientDynamicStyle]}
          resizeMode="stretch"
        />
        <TouchableOpacity
          style={styles.expandTouchableStyle}
          onPress={this.expandContent}
        >
          <Icon name="angle-down" color={colors.primary} size={24}/>
        </TouchableOpacity>
      </View>
    );
  }

  @bind
  onContentLayout({ nativeEvent }) {
    const { height, width } = nativeEvent.layout;
    if (height > this.props.maxHeight) {
      this.setState({ shouldExpand: true, contentWidth: width, contentHeight: height });
    }
    if (this.state.isExpanded || height <= this.props.maxHeight) {
      this.setState({ contentMaxHeight: new Animated.Value(height) });
    }
  }

  @bind
  onLinkPress(url) {
    if (url.startsWith('#')) {
      this.props.onTagPress(url.substring(1));
      return;
    }
    if (url.startsWith('@')) {
      this.props.onUserPress(url.substring(1));
      return;
    }
    this.props.onUrlPress(url);
  }

  render() {
    const { entry, canVote } = this.props;
    const {
      authorAvatarLo, authorSex, author, date, voteCount, userVote, embed, authorGroup, commentCount,
    } = entry;
    let { body } = entry;
    const groupColors = [colors.groupColor0, colors.groupColor1, colors.groupColor2, colors.groupColor3];
    const authorGroupColor = { color: groupColors[authorGroup] };
    body = body.replace(/\n/g, '');
    moment.locale('pl');
    const timeText = moment(date).locale('pl').fromNow();
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.avatarTouchable} onPress={() => this.props.onUserPress(author)}>
          <Avatar
            avatarUrl={authorAvatarLo}
            authorSex={authorSex}
          />
        </TouchableOpacity>
        <View style={styles.content}>
          <View style={styles.header}>
            <View style={styles.userAndTime}>
              <TouchableOpacity onPress={() => this.props.onUserPress(author)}>
                <Text style={[styles.authorText, authorGroupColor]}>{author}</Text>
              </TouchableOpacity>
              <Text style={styles.dateText}>{timeText} | {commentCount} odpowiedzi</Text>
            </View>
            <Button
              disabled={!canVote || userVote === -1}
              success={canVote && userVote !== -1}
              small
              bordered={userVote === 0}
              onPress={() => this.props.onVoteToggle(entry)}
            >
              <Text style={styles.voteText}>+{voteCount}</Text>
            </Button>
          </View>
          <Animated.View
            style={{
              maxHeight: this.state.contentMaxHeight,
              overflow: 'hidden',
            }}
          >
            <View
              onLayout={this.onContentLayout}
            >
              <HTMLView
                value={`<div>${body}</div>`}
                onLinkPress={this.onLinkPress}
                stylesheet={htmlStyles}
              />
              {this.renderExpand()}
            </View>
          </Animated.View>
          <Embed {...this.props} embed={embed}/>
        </View>
      </View>
    );
  }
}

const htmlStyles = StyleSheet.create({
  div: {
    color: colors.textColor,
  },
});

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
  },
  content: {
    flex: 1,
    marginLeft: 8,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 40,
    flex: 1,
  },
  userAndTime: {
    justifyContent: 'space-between',
  },
  authorText: {
    fontWeight: 'bold',
  },
  dateText: {
    color: colors.sideTextColor,
  },
  voteText: {
    color: colors.textColor,
    padding: 3,
  },
  expandContainerStyle: {
    position: 'absolute',
    flex: 1,
    alignItems: 'center',
  },
  expandTouchableStyle: {
    borderRadius: 20,
    overflow: 'hidden',
    borderColor: colors.primary,
    borderWidth: 1,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 3,
  },
  expandGradientStyle: {
    height: 50,
    position: 'absolute',
  },
  avatarTouchable: {
    width: 36,
    height: 36,
  },
});

Content.propTypes = {
  entry: PropTypes.object,
  maxHeight: PropTypes.number,
  onPressImage: PropTypes.func,
  onPressYoutube: PropTypes.func,
  onPressGif: PropTypes.func,
  onPressGfy: PropTypes.func,
  onTagPress: PropTypes.func,
  onUserPress: PropTypes.func,
  onUrlPress: PropTypes.func,
  onVoteToggle: PropTypes.func,
  canVote: PropTypes.bool,
};

Content.defaultProps = {
  maxHeight: null,
  onTagPress: () => null,
  onUserPress: () => null,
  onUrlPress: () => null,
  onVoteToggle: () => null,
};
