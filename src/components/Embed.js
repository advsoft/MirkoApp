import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';
import { bind } from 'decko';
import Video from 'react-native-video';
import ProgressImage from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import colors from '../config/colors';

export default class Embed extends Component {
  state = {
    renderGfy: false,
  };

  @bind
  onPressGfyPreview() {
    const splitUrl = this.props.embed.url.split('/');
    let url = splitUrl[splitUrl.length - 1];
    if (url.endsWith('.webm')) {
      url = url.substring(0, url.length - 5);
    }
    if (url.endsWith('.gif')) {
      url = url.substring(0, url.length - 4);
    }
    const mouseString = '-size_restricted';
    if (url.endsWith(mouseString)) {
      url = url.substring(0, url.length - mouseString.length);
    }
    const videoUrl = `https://giant.gfycat.com/${url}.mp4`;
    this.setState({ renderGfy: true, videoUrl });
  }

  @bind
  onPressGifPreview() {
    this.setState({ renderGif: true });
  }

  renderGif() {
    const { embed } = this.props;
    if (!this.state.renderGif) {
      return null;
    }
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => {
          this.setState({ renderGif: false });
          this.props.onPressGif(embed);
        }}
      >
        <ProgressImage
          style={styles.image}
          indicator={ProgressBar}
          indicatorProps={{
            color: colors.textColor,
            indeterminate: false,
          }}
          source={{ uri: embed.url }}
          resizeMode="contain"
        />
        <TouchableOpacity style={styles.pause} onPress={() => this.setState({ renderGif: false })}>
          <Icon name="pause" color="white" size={16}/>
        </TouchableOpacity>
      </TouchableOpacity>);
  }

  renderGfy() {
    const { embed } = this.props;
    if (!this.state.renderGfy) {
      return null;
    }
    return (

      <TouchableOpacity
        style={styles.container}
        onPress={() => {
          embed.videoUrl = this.state.videoUrl;
          this.props.onPressGfy(embed);
        }}
      >
        <View>
          <Video
            source={{ uri: this.state.videoUrl }} // Can be a URL or a local file.
            ref={(ref) => {
              this.player = ref;
            }} // Store reference
            rate={1.0} // 0 is paused, 1 is normal.
            volume={1.0} // 0 is muted, 1 is normal.
            muted={false} // Mutes the audio entirely.
            paused={false} // Pauses playback entirely.
            resizeMode="contains" // Fill the whole screen at aspect ratio.*
            repeat // Repeat forever.
            playInBackground={false} // Audio continues to play when app entering background.
            style={styles.video}
          />
          <TouchableOpacity style={styles.pause} onPress={() => this.setState({ renderGfy: false })}>
            <Icon name="pause" color="white" size={16}/>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  }

  renderCircleFormat(format) {
    return (
      <View style={styles.circleFormat}>
        <Text style={styles.formatText}>{format}</Text>
      </View>
    );
  }

  renderGifPreview() {
    const { embed } = this.props;
    if (embed.type !== 'image') {
      return null;
    }
    if (this.state.renderGif) {
      return null;
    }
    if (!embed.url.endsWith('.gif')) {
      return null;
    }
    return (
      <TouchableOpacity style={styles.container} onPress={this.onPressGifPreview}>
        <Image style={styles.image} source={{ uri: embed.preview }}/>
      </TouchableOpacity>);
  }


  renderGfyPreview() {
    const { embed } = this.props;
    if (embed.type !== 'video') {
      return null;
    }

    if (!embed.url.includes('gfycat.com')) {
      return null;
    }
    if (this.state.renderGfy) {
      return null;
    }
    return (
      <TouchableOpacity style={styles.container} onPress={this.onPressGfyPreview}>
        <Image style={styles.image} source={{ uri: embed.preview }}/>
        {this.renderCircleFormat('GFY')}
      </TouchableOpacity>);
  }

  renderYoutubePreview() {
    const { embed } = this.props;
    if (embed.type !== 'video') {
      return null;
    }

    if (!embed.url.includes('youtube.com') && !embed.url.includes('youtu.be')) {
      return null;
    }
    return (
      <TouchableOpacity style={styles.container} onPress={() => this.props.onPressYoutube(embed)}>
        <Image style={styles.image} source={{ uri: embed.preview }}/>
        <Image resizeMode="contain" style={styles.youtubeImage} source={require('../../assets/youtube-256.png')}/>
      </TouchableOpacity>);
  }

  renderImagePreview() {
    const { embed } = this.props;
    if (embed.type !== 'image') {
      return null;
    }
    if (embed.url.endsWith('.gif')) {
      return null;
    }
    return (
      <TouchableOpacity style={styles.container} onPress={() => this.props.onPressImage(embed)}>
        <Image style={styles.image} source={{ uri: embed.preview }}/>
      </TouchableOpacity>);
  }

  render() {
    const { embed } = this.props;
    if (!embed) {
      return null;
    }
    return (
      <View>
        {this.renderImagePreview()}
        {this.renderGfyPreview()}
        {this.renderGifPreview()}
        {this.renderYoutubePreview()}
        {this.renderGfy()}
        {this.renderGif()}
      </View>);
  }
}
const previewWidth = 266;
const previewHeight = 200;

const styles = StyleSheet.create({
  container: {
    borderRadius: 2,
    overflow: 'hidden',
    margin: 5,
  },
  image: {
    width: previewWidth,
    height: previewHeight,
    backgroundColor: 'black',
  },
  video: {
    width: previewWidth,
    height: previewHeight,
  },
  pause: {
    padding: 5,
    position: 'absolute',
  },
  circleFormat: {
    width: 50,
    height: 50,
    position: 'absolute',
    borderColor: 'white',
    borderRadius: 25,
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    top: (previewHeight / 2) - 25,
    left: (previewWidth / 2) - 25,
    backgroundColor: 'rgba(33,33,33,0.5)',
  },
  formatText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
  },
  youtubeImage: {
    width: 50,
    height: 50,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    top: (previewHeight / 2) - 25,
    left: (previewWidth / 2) - 25,
  },
});

Embed.propTypes = {
  embed: PropTypes.object,
  onPressImage: PropTypes.func,
  onPressYoutube: PropTypes.func,
  onPressGif: PropTypes.func,
  onPressGfy: PropTypes.func,
};

Embed.defaultProps = {
  onPressImage: () => null,
  onPressYoutube: () => null,
  onPressGif: () => null,
  onPressGfy: () => null,
};

