import React, { Component } from 'react';
import { ActivityIndicator, ScrollView, StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';
import { Button } from 'native-base';
import { bind } from 'decko';
import 'moment/locale/pl';
import colors from '../config/colors';
import Content from './Content';
import CommentsList from './CommentsList';

export default class FullPost extends Component {
  renderSpinner() {
    if (this.props.isLoaded) {
      return null;
    }
    return (
      <ActivityIndicator color="#fff" size="small"/>
    );
  }

  @bind
  onCommentVoteToggle(comment) {
    this.props.onVoteToggle(this.props.entry, comment);
  }

  render() {
    const { entry, maxHeight } = this.props;
    return (
      <ScrollView style={styles.container}>
        <View style={styles.containerInside}>
          <Content {...this.props} entry={entry} maxHeight={maxHeight}/>
          <View style={styles.separator}/>
          {this.renderSpinner()}
          <CommentsList {...this.props} comments={entry.comments} onVoteToggle={this.onCommentVoteToggle}/>
        </View>
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: colors.cardBackground,
    marginHorizontal: 10,
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 3,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
    paddingHorizontal: 5,
  },
  headerContainer: {
    paddingVertical: 5,
  },
  itemContainer: {
    paddingVertical: 5,
    marginLeft: 15,
  },
  activityIndicator: {
    marginTop: 10,
  },
  separator: {
    height: 1,
    flex: 1,
    backgroundColor: colors.inactiveDark,
    marginVertical: 5,
  },
  containerInside: {
    padding: 5,
  },
});

FullPost.propTypes = {
  entry: PropTypes.object,
  isLoaded: PropTypes.bool,
  maxHeight: PropTypes.number,
  onPressImage: PropTypes.func,
  onPressYoutube: PropTypes.func,
  onPressGif: PropTypes.func,
  onPressGfy: PropTypes.func,
  onTagPress: PropTypes.func,
  onUserPress: PropTypes.func,
  onUrlPress: PropTypes.func,
  onVoteToggle: PropTypes.func,
  canVote: PropTypes.bool,
};

