import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import PropTypes from 'prop-types';

export default function LoadingLayer({ loading }) {
  if (!loading) {
    return null;
  }
  return (
    <View style={styles.container}>
      <ActivityIndicator color="#fff" size="large"/>
    </View>
  );
}

LoadingLayer.propTypes = {
  loading: PropTypes.bool.isRequired,
};


const styles = {
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
};
