import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';
import { Button } from 'native-base';
import 'moment/locale/pl';
import colors from '../config/colors';
import Content from './Content';

export default class Post extends Component {
  render() {
    const { entry, maxHeight } = this.props;
    return (
      <View style={styles.container}>
        <Content {...this.props} entry={entry} maxHeight={maxHeight}/>
      </View>);
  }
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: colors.cardBackground,
    marginHorizontal: 10,
    marginTop: 5,
    marginBottom: 5,
    padding: 5,
    borderRadius: 3,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
  },
});

Post.propTypes = {
  entry: PropTypes.object,
  maxHeight: PropTypes.number,
  onPressImage: PropTypes.func,
  onPressYoutube: PropTypes.func,
  onPressGif: PropTypes.func,
  onPressGfy: PropTypes.func,
  onTagPress: PropTypes.func,
  onUserPress: PropTypes.func,
  onUrlPress: PropTypes.func,
  onVoteToggle: PropTypes.func,
  canVote: PropTypes.bool,
};

