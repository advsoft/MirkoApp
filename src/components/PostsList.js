import React, { Component } from 'react';
import { FlatList, TouchableWithoutFeedback, View } from 'react-native';
import PropTypes from 'prop-types';
import { bind } from 'decko';
import Post from './Post';

export default class PostsList extends Component {
  @bind
  renderItem({ item }) {
    return (
      <TouchableWithoutFeedback onPress={() => this.props.onEntryPress(item)}>
        <View>
          <Post
            style={{ marginTop: 10 }}
            entry={item}
            maxHeight={150}
            {...this.props}
          />
        </View>
      </TouchableWithoutFeedback>);
  }

  render() {
    return (<FlatList
      onRefresh={this.props.onRefresh}
      refreshing={this.props.refreshing}
      data={this.props.entries}
      renderItem={this.renderItem}
      keyExtractor={item => item.id}
      onEndReached={this.props.onEndReached}
      onEndReachedThreshold={this.props.onEndReachedThreshold}
    />);
  }
}

PostsList.propTypes = {
  entries: PropTypes.array,
  refreshing: PropTypes.bool,
  onPressImage: PropTypes.func,
  onPressYoutube: PropTypes.func,
  onPressGif: PropTypes.func,
  onPressGfy: PropTypes.func,
  onTagPress: PropTypes.func,
  onUserPress: PropTypes.func,
  onUrlPress: PropTypes.func,
  onEntryPress: PropTypes.func,
  onRefresh: PropTypes.func,
  onEndReached: PropTypes.func,
  onEndReachedThreshold: PropTypes.number,
  onVoteToggle: PropTypes.func,
  canVote: PropTypes.bool,
};

PostsList.defaultProps = {
  onEntryPress: () => null,
  onRefresh: () => null,
  onEndReached: () => null,
  onEndReachedThreshold: 6,
};
