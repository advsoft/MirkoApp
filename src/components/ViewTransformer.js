import React from 'react';

import PropTypes from 'prop-types';

import { createResponder } from 'react-native-gesture-responder';
import OriginalViewTransformer from 'react-native-view-transformer';

export default class ViewTransformer extends OriginalViewTransformer {

  componentWillMount() {
    this.gestureResponder = createResponder({
      onStartShouldSetResponder: (evt, gestureState) => true,
      onMoveShouldSetResponderCapture: (evt, gestureState) => true,
      // onMoveShouldSetResponder: this.handleMove,
      onResponderMove: this.onResponderMove.bind(this),
      onResponderGrant: this.onResponderGrant.bind(this),
      onResponderRelease: this.onResponderRelease.bind(this),
      onResponderTerminate: this.onResponderRelease.bind(this),
      onResponderTerminationRequest: (evt, gestureState) => true, // Do not allow parent view to intercept gesture
    });
  }
}

ViewTransformer.propTypes = {
  /**
   * Use false to disable transform. Default is true.
   */
  enableTransform: PropTypes.bool,

  /**
   * Use false to disable scaling. Default is true.
   */
  enableScale: PropTypes.bool,

  /**
   * Use false to disable translateX/translateY. Default is true.
   */
  enableTranslate: PropTypes.bool,

  /**
   * Default is 20
   */
  maxOverScrollDistance: PropTypes.number,

  maxScale: PropTypes.number,
  contentAspectRatio: PropTypes.number,

  /**
   * Use true to enable resistance effect on over pulling. Default is false.
   */
  enableResistance: PropTypes.bool,

  onViewTransformed: PropTypes.func,

  onTransformGestureReleased: PropTypes.func,
};
ViewTransformer.defaultProps = {
  maxOverScrollDistance: 20,
  enableScale: true,
  enableTranslate: true,
  enableTransform: true,
  maxScale: 1,
  enableResistance: false,
};
