import React, { Component } from 'react';
import { View } from 'react-native';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import ProgressBar from 'react-native-progress/Bar';
import colors from '../config/colors';
import TransformableImage from '../components/TransformableImage';

@observer
export default class ImagePreviewScreen extends Component {
  static navigationOptions = ({ navigation, screenProps }) => ({
    title: '',
  });

  render() {
    return (
      <View style={styles.fullScreen}>
        <TransformableImage
          style={styles.fullScreen}
          source={{ uri: this.props.navigation.state.params.url }}
          indicator={ProgressBar}
          indicatorProps={{
            color: colors.textColor,
            indeterminate: false,
          }}
          androidScaleType="center"
        />
      </View>
    );
  }
}
ImagePreviewScreen.propTypes = {
  navigation: PropTypes.object,
};

const styles = {
  fullScreen: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    overflow: 'hidden',
  },
};
