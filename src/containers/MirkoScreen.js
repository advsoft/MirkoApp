import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { bind } from 'decko';
import { toJS } from 'mobx';
import { denormalize } from 'normalizr';
import mirkoStore from '../stores/mirkoStore';
import BaseMirkoContainer from './abstract/BaseMirkoContainer';
import { entrySchema } from '../schemas/index';

@observer
export default class MirkoScreen extends BaseMirkoContainer {
  static navigationOptions = ({ navigation, screenProps }) => ({
    title: navigation.state.params.title,
  });

  state = {
    refreshing: false,
    entries: [],
    page: 1,
  };

  async init() {
    await this.loadData(1);
  }


  @bind
  async loadData(page) {
    const { tag, author } = this.props.navigation.state.params;
    if (tag) {
      await mirkoStore.fetchTag(tag, page);
    } else {
      await mirkoStore.fetchAuthor(author, page);
    }
    this.setState({ page });
  }

  @bind
  async loadMore() {
    if (this.entries.length === 0) {
      return;
    }
    await this.loadData(this.state.page + 1);
  }

  get entries() {
    const { tag, author } = this.props.navigation.state.params;
    if (tag) {
      return denormalize(mirkoStore.tagEntries[tag], [entrySchema], toJS(mirkoStore));
    }
    return denormalize(mirkoStore.authorEntries[author], [entrySchema], toJS(mirkoStore));
  }

  render() {
    return this.renderPosts(this.entries);
  }
}

MirkoScreen.propTypes = {
  navigation: PropTypes.object,
};
