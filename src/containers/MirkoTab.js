import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import { ActionSheet } from 'native-base';
import get from 'lodash/get';
import { Text, TouchableOpacity } from 'react-native';
import { observer } from 'mobx-react';
import { bind } from 'decko';
import mirkoStore from '../stores/mirkoStore';
import BaseMirkoContainer from './abstract/BaseMirkoContainer';
import colors from '../config/colors';

@observer
export default class MirkoTab extends BaseMirkoContainer {
  static navigationOptions = ({ navigation, screenProps }) => {
    let title = 'Mirko Gorące';

    const hotPeriod = get(navigation, 'state.params.period', mirkoStore.period);
    return ({
      title,
      tabBarLabel: 'Mirko',
      // eslint-disable-next-line react/prop-types
      tabBarIcon: ({ tintColor }) => (<Icon
        name="comments"
        size={20}
        color={tintColor}
      />),
      headerRight: (
        <TouchableOpacity
          style={styles.headerButton}
          onPress={() => navigation.state.params.showHotSelectActionSheet()}
        >
          <Icon
            name="fire"
            size={20}
            color={colors.primary}
          />
          <Text style={styles.headerHotButtonText}>{`${hotPeriod}h`}</Text>
        </TouchableOpacity>),
    });
  };

  @bind
  async init() {
    await mirkoStore.fetchHot();
    this.props.navigation.setParams({ showHotSelectActionSheet: this.showHotSelectActionSheet });
  }

  @bind
  showHotSelectActionSheet() {
    const options = ['6 godzin', '12 godzin', '24 godzin', 'Anuluj'];
    ActionSheet.show({ options, cancelButtonIndex: 3, title: 'Gorące wpisy z ostatnich' }, (buttonIndex) => {
      switch (buttonIndex) {
        case 0:
          this.selectHotPeriod(6);
          break;
        case 1:
          this.selectHotPeriod(12);
          break;
        case 2:
          this.selectHotPeriod(24);
          break;
        default:
      }
    });
  }


  async selectHotPeriod(period) {
    this.setState({ loading: true });
    this.props.navigation.setParams({ period });
    await mirkoStore.fetchHot(1, period);
    this.setState({ loading: false });
  }

  async loadMore() {
    if (mirkoStore.actualHotEntries.length === 0) {
      return;
    }
    await mirkoStore.fetchHot(mirkoStore.page + 1);
  }


  render() {
    return this.renderPosts(mirkoStore.actualHotEntries);
  }
}

const styles = {
  headerButton: {
    padding: 10,
    flexDirection: 'row',
  },
  headerHotButtonText: {
    fontSize: 20,
    marginLeft: 5,
    color: colors.primary,
  },
};

MirkoTab.propTypes = {
  navigation: PropTypes.object,
};
