import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { Linking, View } from 'react-native';
import { denormalize } from 'normalizr';
import { bind } from 'decko';
import { toJS } from 'mobx';
import mirkoStore from '../stores/mirkoStore';
import FullPost from '../components/FullPost';
import { entrySchema } from '../schemas/index';
import authStore from '../stores/authStore';

@observer
export default class PostScreen extends Component {
  static navigationOptions = ({ navigation, screenProps }) => ({});

  state = {
    isLoaded: false,
  };

  componentDidMount() {
    this.init();
  }

  async init() {
    const { entry } = this.props.navigation.state.params;
    await mirkoStore.fetchEntry(entry.id);
    this.setState({ isLoaded: true });
  }

  get entry() {
    const { entry } = this.props.navigation.state.params;
    return denormalize(entry.id, entrySchema, toJS(mirkoStore));
  }

  @bind
  onVoteToggle(entry, comment) {
    if (comment) {
      if (comment.userVote === 0) {
        mirkoStore.voteComment(entry.id, comment.id);
      } else if (comment.userVote === 1) {
        mirkoStore.unvoteComment(entry.id, comment.id);
      }
      return;
    }
    if (entry.userVote === 0) {
      mirkoStore.voteEntry(entry.id);
    } else if (entry.userVote === 1) {
      mirkoStore.unvoteEntry(entry.id);
    }
  }


  render() {
    return (
      <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'flex-start' }}>
        <FullPost
          maxHeight={150}
          entry={this.entry}
          isLoaded={this.state.isLoaded}
          onPressImage={embed => this.props.navigation.navigate('ImagePreview', { url: embed.url })}
          onPressGif={embed => this.props.navigation.navigate('ImagePreview', { url: embed.url })}
          onPressGfy={embed => this.props.navigation.navigate('VideoPreview', { url: embed.videoUrl })}
          onTagPress={tag => this.props.navigation.navigate('Mirko', { title: `Mirko #${tag}`, tag })}
          onUserPress={author => this.props.navigation.navigate('Mirko', { title: `Mirko @${author}`, author })}
          onPressYoutube={embed => Linking.openURL(embed.url)}
          onVoteToggle={this.onVoteToggle}
          canVote={authStore.logged}
        />
      </View>);
  }
}

PostScreen.propTypes = {
  navigation: PropTypes.object,
};
