import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { observer } from 'mobx-react';
import { bind } from 'decko';
import { Alert, Text, View } from 'react-native';
import { Button } from 'native-base';
import mirkoStore from '../stores/mirkoStore';
import Auth from '../components/Auth';
import authStore from '../stores/authStore';
import LoadingLayer from '../components/LoadingLayer';

@observer
export default class ProfileTab extends Component {
  static navigationOptions = ({ navigation, screenProps }) => {
    let title = 'Mirko';
    if (mirkoStore.type === 'hot') {
      title += ' Gorące';
    }
    if (mirkoStore.type === 'tag') {
      title += ` #${mirkoStore.tag}`;
    }
    return ({
      title: 'Profile',
      tabBarLabel: 'Profile',
      tabBarIcon: ({ tintColor }) => (<Icon
        name="user"
        size={20}
        color={tintColor}
      />),
    });
  };
  state = {
    processing: false,
  };

  @bind
  async onLogin(username, password) {
    this.setState({ processing: true });
    try {
      await authStore.login(username, password);
    } catch (error) {
      this.setState({ processing: false });
      if (!error.authError) {
        Alert.alert('Niespodziewany błąd logowania.');
      }
      return;
    }
    this.setState({ processing: false });
    Alert.alert('Zalogowano pomyślnie');
  }

  onLogOut() {
    authStore.logout();
    Alert.alert('Zostałeś wylogowany');
  }


  renderAuth() {
    if (authStore.logged) {
      return (
        <Button
          style={styles.signOutButton}
          danger
          bordered
          onPress={this.onLogOut}
        >
          <Text style={styles.signOutButtonText}>
            Wyloguj
          </Text>
        </Button>
      );
    }
    return (
      <Auth onLogin={this.onLogin} error={authStore.error}/>
    );
  }


  render() {
    return (

      <View style={styles.container}>
        {this.renderAuth()}
        <LoadingLayer loading={this.state.processing}/>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  signOutButton: {
    marginTop: 20,
    marginHorizontal: 10,
  },
  signOutButtonText: {
    color: 'white',
    flex: 1,
    textAlign: 'center',
  },
};

