import React, { Component } from 'react';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import { ActionSheet, Button, Container, Content, Header, Icon, Input, Item, Segment, Text } from 'native-base';
import { denormalize } from 'normalizr';
import { toJS } from 'mobx';
import { Alert } from 'react-native';
import get from 'lodash/get';
import { observer } from 'mobx-react';
import { bind } from 'decko';
import mirkoStore from '../stores/mirkoStore';
import BaseMirkoContainer from './abstract/BaseMirkoContainer';
import colors from '../config/colors';
import { entrySchema } from '../schemas/index';

@observer
export default class SearchTab extends BaseMirkoContainer {
  static navigationOptions = ({ navigation, screenProps }) => {
    const title = 'Szukaj';

    const hotPeriod = get(navigation, 'state.params.period', mirkoStore.period);
    return ({
      title,
      header: null,
      tabBarLabel: 'Szukaj',
      // eslint-disable-next-line react/prop-types
      tabBarIcon: ({ tintColor }) => (<FontAwesomeIcon
        name="search"
        size={20}
        color={tintColor}
      />),
    });
  };
  state = {
    searchType: 'tag',
    search: '',
    ...this.state,
  };

  @bind
  async init() {
  }

  @bind
  async onSearch() {
    this.setState({ loading: true });
    await this.loadData(1);
    this.setState({ loading: false });
  }

  @bind
  async loadData(page) {
    const { searchType, search } = this.state;
    try {
      if (searchType === 'tag') {
        await mirkoStore.fetchTag(search, page);
      } else {
        await mirkoStore.fetchAuthor(search, page);
      }
    } catch (error) {
      Alert.alert(error.message);
    }
    this.setState({ page });
  }

  @bind
  async loadMore() {
    if (this.entries.length === 0) {
      return;
    }
    await this.loadData(this.state.page + 1);
  }

  get entries() {
    const { searchType, search } = this.state;
    if (searchType === 'tag') {
      return denormalize(mirkoStore.tagEntries[search], [entrySchema], toJS(mirkoStore));
    }
    return denormalize(mirkoStore.authorEntries[search], [entrySchema], toJS(mirkoStore));
  }


  render() {
    return (
      <Container>
        <Header
          style={{ backgroundColor: '#fff' }}
          iosBarStyle="light-content"
          backgroundColor={colors.cardBackground}
          searchBar
          rounded
          hasSegment
        >
          <Item>
            <Icon name="search"/>
            <Input
              style={{ color: colors.inverseTextColor }}
              placeholder="Search"
              autoCapitalize="none"
              value={this.state.search}
              onChangeText={search => this.setState({ search })}
            />
            <Icon theme={{ iconFamily: 'FontAwesome' }} name={this.state.searchType === 'tag' ? 'hashtag' : 'user'}/>
          </Item>
          <Button transparent onPress={this.onSearch}>
            <Text style={{ color: colors.textColor }}>Search</Text>
          </Button>
        </Header>
        <Segment>
          <Button
            first
            active={this.state.searchType === 'tag'}
            onPress={() => this.setState({ searchType: 'tag' })}
          >
            <Text>Tag</Text>
          </Button>
          <Button
            last
            active={this.state.searchType === 'author'}
            onPress={() => this.setState({ searchType: 'author' })}
          >
            <Text>Autor</Text>
          </Button>
        </Segment>
        {this.renderPosts(this.entries)}
      </Container>
    );
  }
}

const styles = {
  headerButton: {
    padding: 10,
    flexDirection: 'row',
  },
  headerHotButtonText: {
    fontSize: 20,
    marginLeft: 5,
    color: colors.primary,
  },
};

SearchTab.propTypes = {
  navigation: PropTypes.object,
};
