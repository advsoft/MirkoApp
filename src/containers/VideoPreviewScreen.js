import React, { Component } from 'react';
import { View } from 'react-native';
import { observer } from 'mobx-react';
import ProgressBar from 'react-native-progress/Bar';
import Video from 'react-native-video';
import PropTypes from 'prop-types';

@observer
export default class VideoPreviewScreen extends Component {
  static navigationOptions = ({ navigation, screenProps }) => ({
    title: '',
  });
  state = {
    isLoading: false,
    progress: 0,
  };


  renderProgresIndicator() {
    const { progress, isLoading } = this.state;
    if (!isLoading) {
      return null;
    }
    return <ProgressBar progress={progress}/>;
  }

  render() {
    return (
      <View style={styles.fullScreen}>
        <Video
          source={{ uri: this.props.navigation.state.params.url }} // Can be a URL or a local file.
          ref={(ref) => {
            this.player = ref;
          }}
          rate={1.0}
          volume={1.0}
          muted={false}
          paused={false}
          resizeMode="contains"
          repeat
          playInBackground={false}
          style={styles.fullScreen}
        />
      </View>
    );
  }
}

VideoPreviewScreen.propTypes = {
  navigation: PropTypes.object,
};

const styles = {
  fullScreen: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
};
