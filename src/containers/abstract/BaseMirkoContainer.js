import React, { Component } from 'react';
import { ActivityIndicator, Linking, View } from 'react-native';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { bind } from 'decko';
import PropTypes from 'prop-types';
import PostsList from '../../components/PostsList';
import colors from '../../config/colors';
import mirkoStore from '../../stores/mirkoStore';
import authStore from '../../stores/authStore';

@observer
export default class BaseMirkoContainer extends Component {
  constructor() {
    super();
    if (new.target === BaseMirkoContainer) {
      throw new TypeError('Cannot construct Abstract instances directly');
    }
  }

  state = {
    refreshing: false,
    loading: false,
  };


  async componentWillMount() {
    this.setState({ loading: true });
    await this.init();
    this.setState({ loading: false });
  }

  async init() {
    throw new Error('UnimplementedException');
  }


  @bind
  async onRefresh() {
    this.setState({ refreshing: true });
    await this.init();
    this.setState({ refreshing: false });
  }

  renderSpinner() {
    if (!this.state.loading) {
      return null;
    }
    return (
      <View style={styles.spinnerStyle}>
        <ActivityIndicator color={colors.textColor} size="small"/>
      </View>
    );
  }

  @bind
  onEndReached() {
    this.loadMore();
  }

  @bind
  onEntryPress(entry) {
    this.props.navigation.navigate('Post', { entry });
  }

  @bind
  onVoteToggle(entry) {
    if (entry.userVote === 0) {
      mirkoStore.voteEntry(entry.id);
    } else if (entry.userVote === 1) {
      mirkoStore.unvoteEntry(entry.id);
    }
  }

  renderPosts(entries) {
    return (
      <View style={styles.postListContainer}>
        {this.renderSpinner()}
        <PostsList
          refreshing={this.state.refreshing}
          entries={toJS(entries)}
          onPressImage={embed => this.props.navigation.navigate('ImagePreview', { url: embed.url })}
          onPressGif={embed => this.props.navigation.navigate('ImagePreview', { url: embed.url })}
          onPressGfy={embed => this.props.navigation.navigate('VideoPreview', { url: embed.videoUrl })}
          onTagPress={tag => this.props.navigation.navigate('Mirko', { title: `Mirko #${tag}`, tag })}
          onUserPress={author => this.props.navigation.navigate('Mirko', { title: `Mirko @${author}`, author })}
          onPressYoutube={embed => Linking.openURL(embed.url)}
          onRefresh={this.onRefresh}
          onEndReached={this.onEndReached}
          onEntryPress={this.onEntryPress}
          onVoteToggle={this.onVoteToggle}
          canVote={authStore.logged}
        />
      </View>);
  }
}

BaseMirkoContainer.propTypes = {
  navigation: PropTypes.object,
};

const styles = {
  postListContainer: {
    flex: 1,
  },
  spinnerStyle: {
    marginTop: 20,
  },
};
