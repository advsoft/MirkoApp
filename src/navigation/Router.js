import { TabNavigator, StackNavigator } from 'react-navigation';
import MirkoTab from '../containers/MirkoTab';
import colors from '../config/colors';
import ImagePreviewScreen from '../containers/ImagePreviewScreen';
import VideoPreviewScreen from '../containers/VideoPreviewScreen';
import MirkoScreen from '../containers/MirkoScreen';
import ProfileTab from '../containers/ProfileTab';
import PostScreen from '../containers/PostScreen';
import SearchTab from "../containers/SearchTab";

const headerStyle = {
  headerTintColor: colors.textColor,
  headerStyle: {
    backgroundColor: colors.cardBackground,
  },
};

const TabRouter = TabNavigator({
  MirkoTab: { screen: MirkoTab },
  SearchTab: { screen: SearchTab },
  ProfileTab: { screen: ProfileTab },
}, {
  tabBarPosition: 'bottom',
  tabBarOptions: {
    activeTintColor: colors.primary,
    inactiveTintColor: colors.inactive,
    showIcon: true,
    style: {
      backgroundColor: colors.cardBackground,
    },

  },
  swipeEnabled: false,
});

const StackRouter = StackNavigator({
  Tab: { screen: TabRouter, navigationOptions: { ...headerStyle } },
  ImagePreview: { screen: ImagePreviewScreen, navigationOptions: { ...headerStyle } },
  VideoPreview: { screen: VideoPreviewScreen, navigationOptions: { ...headerStyle } },
  Mirko: { screen: MirkoScreen, navigationOptions: { ...headerStyle } },
  Post: { screen: PostScreen, navigationOptions: { ...headerStyle } },
}, {
  cardStyle: {
    backgroundColor: colors.background,
  },
  headerMode: 'screen',
  headerStyle: {
    backgroundColor: colors.cardBackground,
  },
});

export { TabRouter, StackRouter };
