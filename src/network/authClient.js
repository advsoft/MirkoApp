import axiosOrg from 'axios';
import {
  addAppKey,
  error,
  jsonDataToFormData,
  signRequest,
  storeOriginalConfig,
  successCamelize,
  successDecamelize
} from './interceptors';
import appConfig from '../config/appConfig';
import { addUserKey, generateRefreshTokenInterceptor } from './authInterceptor';


const client = axiosOrg.create({
  baseURL: appConfig.url,
  timeout: 10000,
});



client.interceptors.request.use(jsonDataToFormData, error);
client.interceptors.request.use(signRequest, error);
client.interceptors.request.use(addUserKey, error);
client.interceptors.request.use(addAppKey, error);
client.interceptors.request.use(successDecamelize, error);
client.interceptors.request.use(storeOriginalConfig, error);
client.interceptors.response.use(successCamelize, error);
client.interceptors.response.use(generateRefreshTokenInterceptor(client), error);

export default client;
