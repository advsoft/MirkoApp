import get from 'lodash/get';
import authStore from '../stores/authStore';

// eslint-disable-next-line import/prefer-default-export
export function addUserKey(config) {
  if (authStore.logged) {
    // eslint-disable-next-line no-param-reassign
    config.url += `,userkey,${authStore.userKey}`;
  }
  return config;
}

export function generateRefreshTokenInterceptor(authClient) {
  return async function refreshToken(response) {
    if (response.status !== 200) {
      return response;
    }
    const errorCode = get(response, 'data.error.code');
    if (errorCode !== 11) {
      return response;
    }
    await authStore.loginApi();
    return authClient(response.config.originalConfig);
  };
}
