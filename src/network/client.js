import axiosOrg from 'axios';
import { successDecamelize, successCamelize, error, addAppKey, signRequest, jsonDataToFormData } from './interceptors';
import appConfig from '../config/appConfig';


const client = axiosOrg.create({
  baseURL: appConfig.url,
  timeout: 10000,
});
client.interceptors.request.use(jsonDataToFormData, error);
client.interceptors.request.use(signRequest, error);
client.interceptors.request.use(addAppKey, error);
client.interceptors.request.use(successDecamelize, error);
client.interceptors.response.use(successCamelize, error);

export default client;
