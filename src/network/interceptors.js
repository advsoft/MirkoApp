/* eslint-disable no-param-reassign */
import { camelizeKeys, decamelizeKeys } from 'humps';
import md5 from 'crypto-js/md5';
import appConfig from '../config/appConfig';

export function successCamelize(response) {
  response.data = camelizeKeys(response.data);
  return response;
}

export function successDecamelize(config) {
  config.data = decamelizeKeys(config.data);
  return config;
}

export function error(error) {
  return Promise.reject(error);
}

export function addAppKey(config) {
  config.url += `appkey,${appConfig.appKey}`;
  return config;
}

export function signRequest(config) {
  let data = '';
  if (config.data) {
    const dataKeys = Object.keys(config.data);
    dataKeys.sort();
    const values = [];
    for (const key of dataKeys) {
      values.push(config.data[key]);
    }
    data = values.join();
  }
  const content = appConfig.appSecret + config.baseURL + config.url + data;
  config.headers.apisign = md5(content).toString();
  return config;
}

export function jsonDataToFormData(config) {
  if (!config.data) {
    return config;
  }
  const form = new FormData();
  // eslint-disable-next-line guard-for-in
  for (const key in config.data) {
    form.append(key, config.data[key]);
  }
  config.data = form;
  return config;
}

export function storeOriginalConfig(config) {
  config.originalConfig = { ...config };
  return config;
}
