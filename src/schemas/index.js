import { schema } from 'normalizr';

export const commentSchema = new schema.Entity('comments');

export const entrySchema = new schema.Entity('entries', {
  comments: [commentSchema],
});
