import { action, computed, observable, toJS } from 'mobx';
import CookieManager from 'react-native-cookies';
import { persist } from 'mobx-persist';
import client from '../network/client';
import profileStore from './profileStore';

class AuthStore {
  @observable @persist username = null;
  @observable @persist accountKey = null;
  @observable @persist userKey = null;
  @observable error = null;

  @computed
  get logged() {
    return this.userKey !== null;
  }

  @action
  logout() {
    this.username = null;
    this.accountKey = null;
    this.userKey = null;
    this.error = null;
    profileStore.profile = null;
  }

  @action
  async login(username, password) {
    this.error = null;
    await this.loginWeb(username, password);
    this.username = username;
    const html = await this.getConnectPageHtml();
    let accountKey = await this.getAccountKey(html);
    if (accountKey === null) {
      const connectUrl = await this.getConnectUrl(html);
      const token = await this.getTokenToConnect(connectUrl);
      accountKey = await this.connectToApp(connectUrl, token);
    }
    this.accountKey = accountKey;
    await this.loginApi();
  }

  async loginApi() {
    const response = await client({
      url: 'user/login/',
      method: 'POST',
      data: { login: this.username, accountkey: this.accountKey },
    });
    const { data } = response;
    this.userKey = data.userkey;
    profileStore.profile = data;
    return response.data;
  }


  async loginWeb(username, password) {
    await CookieManager.clearAll();
    const form = new FormData();
    form.append('user[username]', username);
    form.append('user[password]', password);

    const response = await fetch('https://www.wykop.pl/zaloguj/', {
      method: 'POST',
      body: form,
      credentials: 'include',
    });
    if (response.url !== 'https://www.wykop.pl/') {
      this.error = 'Błędny login lub hasło!';
      throw { authError: true, message: this.error };
    }
  }

  getAccountKey(html) {
    const input = '<input type="text" value="';
    const index = html.indexOf(input);
    if (index === -1) { return null; }
    const endIndex = html.indexOf('"', index + input.length);
    return html.substring(index + input.length, endIndex);
  }

  async getConnectPageHtml() {
    const response = await fetch('https://www.wykop.pl/dodatki/pokaz/1007/', {
      method: 'GET',
      credentials: 'include',
    });

    return response.text();
  }

  async getConnectUrl(html) {
    const index = html.indexOf('https://www.wykop.pl/dodatki/appconnect/');
    const endIndex = html.indexOf('"', index);
    return html.substring(index, endIndex);
  }

  async getTokenToConnect(url) {
    const response = await fetch(url, {
      method: 'GET',
      credentials: 'include',
    });
    const inputId = '__token';
    const valueField = 'value="';
    const html = await response.text();
    const firstIndex = html.indexOf(inputId);
    const valueIndex = html.indexOf(valueField, firstIndex) + valueField.length;
    const endIndex = html.indexOf('"', valueIndex);
    const token = html.substring(valueIndex, endIndex);
    return token;
  }

  async connectToApp(url, token) {
    const form = new FormData();
    form.append('connect[permissions][]', 'login');
    form.append('connect[permissions][]', 'votes');
    form.append('connect[permissions][]', 'microblog');
    form.append('connect[permissions][]', 'comments');
    form.append('connect[permissions][]', 'links');
    form.append('connect[permissions][]', 'profile');
    form.append('connect[permissions][]', 'pm');
    form.append('__token', token);
    form.append('appnew[submit]', 'Połącz z aplikacją');
    const response = await fetch(url, {
      method: 'POST',
      body: form,
      credentials: 'include',
    });
    const html = await response.text();
    return this.getAccountKey(html);
  }
}

export default new AuthStore();
