import { action, computed, observable, toJS } from 'mobx';
import { persist } from 'mobx-persist';
import { denormalize, normalize } from 'normalizr';
import client from '../network/authClient';
import { entrySchema } from '../schemas';
import merge from '../utils/merge';


const TYPE_HOT = 'hot';

class MirkoStore {
  @observable entries = {};

  @observable comments = {};

  @observable hotEntries = {};
  @observable tagEntries = {};
  @observable authorEntries = {};


  @observable @persist period = 12;

  @observable page = 1;

  @computed
  get actualHotEntries() {
    return denormalize(this.hotEntries[this.period], [entrySchema], toJS(this));
  }

  @action
  async fetchHot(page = 1, period = this.period) {
    if (page === 1) {
      this.hotEntries[period] = [];
    }
    this.type = TYPE_HOT;
    this.period = period;
    this.page = page;
    const response = await client({ url: `stream/hot/page,${page},period,${period},`, method: 'get' });
    const normalized = normalize(response.data, [entrySchema]);
    this.hotEntries[period].push(...normalized.result);
    this.entries = merge(this.entries, normalized.entities.entries);
  }

  @action
  async fetchEntry(entryId) {
    const response = await client({ url: `entries/${entryId}/`, method: 'get' });
    const normalized = normalize(response.data, entrySchema);
    this.comments = merge(this.comments, normalized.entities.comments);
    this.entries = merge(this.entries, normalized.entities.entries);
    return response.data;
  }

  @action
  async fetchTag(tag = 'youtube', page = 1) {
    if (page === 1) {
      this.tagEntries[tag] = [];
    }
    const response = await client({ url: `tag/entries/${tag}/page,${page},`, method: 'get' });
    if (response.data.error) {
      return;
    }
    const normalized = normalize(response.data.items, [entrySchema]);
    this.tagEntries[tag].push(...normalized.result);
    if (normalized.entities.entries) {
      this.entries = merge(this.entries, normalized.entities.entries);
    } else {
      throw { message: 'Wybrany tag nie zawiera wpisów' };
    }
  }

  @action
  async fetchAuthor(author = 'noipoptakach', page = 1) {
    if (page === 1) {
      this.authorEntries[author] = [];
    }
    const response = await client({ url: `profile/entries/${author}/page,${page},`, method: 'get' });
    if (response.data.error) {
      throw { message: response.data.error.message };
    }
    const normalized = normalize(response.data, [entrySchema]);
    this.authorEntries[author].push(...normalized.result);
    this.entries = merge(this.entries, normalized.entities.entries);
  }

  @action
  async voteEntry(entryId) {
    this.entries[entryId].userVote = -1;
    const response = await this.vote('entry', entryId);
    this.entries[entryId].voteCount = response.vote;
    this.entries[entryId].userVote = 1;
  }

  @action
  async voteComment(entryId, commentId) {
    this.comments[commentId].userVote = -1;
    const response = await this.vote('comment', entryId, commentId);
    this.comments[commentId].voteCount = response.vote;
    this.comments[commentId].userVote = 1;
  }


  @action
  async vote(type, entryId, commentId) {
    let url = `entries/vote/${type}/${entryId}/`;
    if (commentId) {
      url += `${commentId}/`;
    }
    const response = await client({ url, method: 'get' });
    return response.data;
  }

  @action
  async unvoteEntry(entryId) {
    this.entries[entryId].userVote = -1;
    const response = await this.unvote('entry', entryId);
    this.entries[entryId].voteCount = response.vote;
    this.entries[entryId].userVote = 0;
  }

  @action
  async unvoteComment(entryId, commentId) {
    this.comments[commentId].userVote = -1;
    const response = await this.unvote('comment', entryId, commentId);
    this.comments[commentId].voteCount = response.vote;
    this.comments[commentId].userVote = 0;
  }

  @action
  async unvote(type, entryId, commentId) {
    let url = `entries/unvote/${type}/${entryId}/`;
    if (commentId) {
      url += `${commentId}/`;
    }
    const response = await client({ url, method: 'get' });
    return response.data;
  }
}

export default new MirkoStore();
