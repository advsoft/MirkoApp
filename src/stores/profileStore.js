import { observable } from 'mobx';
import { persist } from 'mobx-persist';
import { client } from '../network/client';

class ProfileStore {
  @observable @persist('object') profile = null;
}

export default new ProfileStore();
