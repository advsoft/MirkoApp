import deepmerge from 'deepmerge';


function overwriteMerge(destinationArray, sourceArray, options) {
  return sourceArray;
}

export default function merge(first, second) {
  if (!first) {
    return second;
  }
  if (!second) {
    return first;
  }
  return deepmerge(first, second, { arrayMerge: overwriteMerge });
}
